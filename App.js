import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default function App() {
  const [text, setText] = React.useState('Default Text')
  return (
    <View style={styles.container}>
      <Text>{text}</Text>
      <Button title='Change Text' onPress={() => setText('Button pressed')} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
